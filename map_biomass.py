
import csv
import itertools
import matplotlib.pyplot as plt
import multiprocessing
import numpy as np
import os
from osgeo import gdal
import pdb

import biota





def writeSMCSV(tile_t1, tile_t2, output_dir = os.getcwd()):
    '''
    Output a sub-sample of SM and Gamma0 to a CSV file
    '''
    
    # Get gamma0 and soil moisture, and 
    AGB_t1 = tile_t1.getAGB()[::100, ::100]
    AGB_t2 = tile_t2.getAGB()[::100, ::100]
    sm_t1 = tile_t1.getSM()[::100, ::100]
    sm_t2 = tile_t2.getSM()[::100, ::100]
    
    mask = AGB_t1.mask | AGB_t2.mask | sm_t1.mask | sm_t2.mask
    
    AGB_t1_out = AGB_t1.data[mask == False].tolist()
    AGB_t2_out = AGB_t2.data[mask == False].tolist()
    sm_t1_out = sm_t1.data[mask == False].tolist()
    sm_t2_out = sm_t2.data[mask == False].tolist()
    
    tile_change = biota.LoadChange(tile_t1, tile_t2)
    
    # Output sample to CSV
    with open('%s/%s'%(output_dir, (tile_change.output_pattern%'SM').split('.')[0] + '.csv'), 'wb') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['lat','lon','AGB_%s'%str(tile_t1.year), 'AGB_%s'%str(tile_t2.year), 'SM_%s'%str(tile_t1.year), 'SM_%s'%str(tile_t2.year)])
        
        for bt1, bt2, st1, st2 in zip(AGB_t1_out, AGB_t2_out, sm_t1_out, sm_t2_out):
            writer.writerow([str(tile_t1.lat), str(tile_t1.lon), str(bt1), str(bt2), str(st1), str(st2)])

def main(latlons):
    '''
    '''
       
    lat, lon = latlons
    
    print 'Doing lat: %s, lon: %s'%(str(lat).zfill(2), str(lon).zfill(3))
    
    ## Inputs
    data_dir = '/home/sbowers3/SMFM/ALOS_data/'
    CCI_landcover_2007 = '/home/sbowers3/SMFM/ESA_CCI/ESACCI-LC-L4-LCCS-Map-300m-P1Y-2007-v2.0.7.tif'
    CCI_landcover_2010 = '/home/sbowers3/SMFM/ESA_CCI/ESACCI-LC-L4-LCCS-Map-300m-P1Y-2010-v2.0.7.tif'
    hansen_water = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/hansen_water/hansen_water_mask.vrt'
    hansen_loss = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/hansen_loss/hansen_loss_year.vrt'
    hansen_treecover = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/hansen_treecover/hansen_treecover.vrt'
    sm_dir = '/home/sbowers3/guasha/sam_bowers/soil_moisture/'
    output_dir = '/home/sbowers3/guasha/sam_bowers/carla_outputs/'
    
    # Years
    t1 = 2007
    t2 = 2010
    
    # Postprocessing options
    lee_filter = True
    downsample_factor = 1
    CCI_classes = [0, 10, 11, 12, 30, 40, 50, 190, 200, 201, 202, 220]
    CCI_classes_buffer = [20, 160, 170, 180, 210]
    
    ## Load data
    try:
        tile_t1 = biota.LoadTile(data_dir, lat, lon, t1, lee_filter = lee_filter, downsample_factor = downsample_factor, sm_dir = sm_dir, output_dir = output_dir)
        tile_t2 = biota.LoadTile(data_dir, lat, lon, t2, lee_filter = lee_filter, downsample_factor = downsample_factor, sm_dir = sm_dir, output_dir = output_dir)
    except:
        return
    
    # Mask out wetland/anthropogenic classes from CCI dataset. Add a buffer to some of them.
    tile_t1.updateMask(CCI_landcover_2007, classes = CCI_classes)
    tile_t1.updateMask(CCI_landcover_2007, classes = CCI_classes_buffer, buffer_size = 1000)
    tile_t1.updateMask(CCI_landcover_2010, classes = CCI_classes)
    tile_t1.updateMask(CCI_landcover_2010, classes = CCI_classes_buffer, buffer_size = 1000)
    
    tile_t2.updateMask(CCI_landcover_2007, classes = CCI_classes)
    tile_t2.updateMask(CCI_landcover_2007, classes = CCI_classes_buffer, buffer_size = 1000)
    tile_t2.updateMask(CCI_landcover_2010, classes = CCI_classes)
    tile_t2.updateMask(CCI_landcover_2010, classes = CCI_classes_buffer, buffer_size = 1000)
    
    # Mask out rivers with a buffer arond the Hansen data mask
    tile_t1.updateMask(hansen_water, classes = [2], buffer_size = 1000)
    tile_t2.updateMask(hansen_water, classes = [2], buffer_size = 1000)
    
    # Mask out land cover change for the period 2007 to 2010 from Hansen data
    tile_t1.updateMask(hansen_loss, classes = range(tile_t1.year - 2000, tile_t2.year + 1 - 2000, 1), buffer_size = 250)
    tile_t2.updateMask(hansen_loss, classes = range(tile_t1.year - 2000, tile_t2.year + 1 - 2000, 1), buffer_size = 250)
        
    # Output CSV of soil moisture and AGB
    writeSMCSV(tile_t1, tile_t2, output_dir = output_dir)       
    
    # Load a change object, and calculate soil moisture change
    tile_change = biota.LoadChange(tile_t1, tile_t2, output_dir = output_dir)
    
    # Output AGB and AGB change (uncorrected)
    AGB_t2 = tile_t2.getAGB(output = True)
    #AGB_change = tile_change.getAGBChange(output = True)
    
    # SM correction to AGB_t2 where SM data is available (assume no interaction model is the best)
    tile_t2.AGB[tile_change.getSMChange().mask == False] = (tile_t2.getAGB() - (7.8484 * tile_change.getSMChange()))[tile_change.getSMChange().mask == False]

    # Reload tile_change object with updated AGB
    tile_change = biota.LoadChange(tile_t1, tile_t2, output_dir = output_dir)
    
    """
    # Build a mask of areas with > 10 % and < -10 % soil moisture change
    sm_mask = np.logical_or(tile_change.getSMChange() > 0.1, tile_change.getSMChange() < -0.1)
    
    # Mask out areas of significant soil moisture change
    tile_t1.updateMask(sm_mask.data | sm_mask.mask)
    tile_t2.updateMask(sm_mask.data | sm_mask.mask)
    tile_change.updateMask(sm_mask.data | sm_mask.mask)
    """
    
    # Calculate AGB/AGB change and output
    AGB_t1 = tile_t1.getAGB(output = True)
    
    # Output SM adjusted AGB_t2 and AGBChange
    biota.IO.outputGeoTiff(tile_t2.getAGB(), tile_t2.output_pattern%'AGBadj', tile_t2.geo_t, tile_t2.proj, output_dir = output_dir, nodata = 999999)
    #biota.IO.outputGeoTiff(tile_change.getAGBChange(), tile_change.output_pattern%'AGBChangeadj', tile_change.geo_t, tile_change.proj, output_dir = output_dir)

    # Calculate and output soil moisture change
    sm_change = tile_change.getSMChange(output = True)
    
    # Load Hansen treecover data, and apply mask
    treecover = biota.IO.loadRaster(hansen_treecover, tile_t1, resampling = gdal.GRA_Average)
    treecover = np.ma.array(treecover, mask = tile_t1.mask, dtype=np.uint8)
    
    # And output
    biota.IO.outputGeoTiff(treecover, (tile_t1.output_pattern%'treecover').replace('_2007',''), tile_t1.geo_t, tile_t1.proj, output_dir = output_dir, dtype = gdal.GDT_Byte, nodata = 255)
    
    # Scatterplot
    #plt.scatter(AGB_px, change_px)
    #plt.show()
    
    # Boxplot
    # build_boxplot(AGB_t1, AGB_change)
    
    # Hartigan's dip test
    #import diptest.diptest
    #dip_stat, p = diptest.diptest.diptest(AGB_px)


if __name__ == '__main__':
    '''
    Parallelised
    '''   
    
    lats = range(-35, 1) #range(-15,-5) #
    lons = range(0, 46) #range(30,40) #range(0, 45)
    
    latlons = [list(i) for i in itertools.product(lats,lons)]
    
    #main((-15,30))
    pool = multiprocessing.Pool(50)
    
    pool.map(main, latlons)
    
    pool.close()
