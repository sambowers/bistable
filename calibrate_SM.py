import glob
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats

import pdb

def loadCSV(data_dir, t1, t2, downsample = 1):
    '''
    Function to load SM/AGB .csv file into numpy arrays
    
    Args:
        data_dir: Directory containing csv files
        t1: year 1
        t2: year 2
        downsample: Reduce data volume by this factor. Defalts to 1
    Returns:
        AGB_t1, gamm0_t2, sm_t1, sm_t2
    '''
    
    csv_files = sorted(glob.glob('%s/SM*_%s_%s_*.csv'%(data_dir, str(t1), str(t2))))

    AGB_t1 = []
    AGB_t2 = []
    sm_t1 = []
    sm_t2 = []

    for csv_file in csv_files:
        df = pd.read_csv(csv_file, sep=',')
        AGB_t1.extend(df['AGB_%s'%str(t1)].tolist())
        AGB_t2.extend(df['AGB_%s'%str(t2)].tolist())
        sm_t1.extend(df['SM_%s'%str(t1)].tolist())
        sm_t2.extend(df['SM_%s'%str(t2)].tolist())
    
    AGB_t1 = np.array(AGB_t1)[::downsample]
    AGB_t2 = np.array(AGB_t2)[::downsample]
    sm_t1 = np.array(sm_t1)[::downsample]
    sm_t2 = np.array(sm_t2)[::downsample]
    
    return AGB_t1, AGB_t2, sm_t1, sm_t2


def scatterPlot1(AGB_t1, AGB_t2, sm_t1, sm_t2):
    '''
    Build a SM/AGB scatter plot with fitted line
    '''

    slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(sm_t2 - sm_t1, AGB_t2 - AGB_t1)
    
    fig, ax = plt.subplots()
    ax.scatter(sm_t2 - sm_t1, AGB_t2 - AGB_t1, s=5, edgecolor='', c='darkblue')
    ax.plot(np.arange(-5,5,0.01), np.arange(-5,5,0.01)*slope + intercept, c='k')
    ax.set_xlim([-0.15, 0.15])
    ax.set_ylim([-10, 10])
    ax.set_xlabel('Soil moisture change (m$^2$/m$^2$)')
    ax.set_ylabel('AGB change')
    plt.show()
    

def scatterPlot2(AGB_t1, AGB_t2, sm_t1, sm_t2):
    '''
    Build a SM/AGB scatter plot with fitted line
    '''

    from sklearn.linear_model import LinearRegression
    interaction = ((sm_t2 - sm_t1) * AGB_t1)[:,np.newaxis]
    regr = LinearRegression()
    model = regr.fit(interaction, AGB_t2 - AGB_t1)
        
    fig, ax = plt.subplots()
    ax.scatter(model.predict(interaction), AGB_t2 - AGB_t1, s=5, edgecolor='', c='darkblue')
    ax.plot(np.arange(-5,5,0.01)*model.coef_, np.arange(-5,5,0.01), c='k')
    ax.set_xlabel('Soil moisture change (m$^2$/m$^2$)')
    ax.set_ylabel('AGB change')
    plt.show()
    

def scatterPlot3(AGB_t1, AGB_t2, sm_t1, sm_t2):
    '''
    Build a SM/AGB scatter plot with fitted line.
    '''
    
    import patsy
    df = pd.DataFrame(data = np.vstack((AGB_t1, AGB_t2, AGB_t2 - AGB_t1, sm_t1, sm_t2, sm_t2 - sm_t1)).T, columns=np.array(['AGB_t1', 'AGB_t2', 'AGB_change', 'sm_t1', 'sm_t2', 'sm_change']))
    
    import statsmodels.formula.api as smf           
    mod1 = smf.ols(formula='AGB_change ~ sm_change + sm_change:AGB_t1 -1', data=df).fit()
    mod2 = smf.ols(formula='AGB_change ~ sm_change -1', data=df).fit()
    mod3 = smf.ols(formula='AGB_change ~ sm_change:AGB_t1 -1', data=df).fit()
    
    # Assume model 2 the best (wins by p-values, R2, and AIC/BIC)
    print mod2.summary()
    
    fig, ax = plt.subplots()
    ax.scatter(sm_t2 - sm_t1, AGB_t2 - AGB_t1, s=5, edgecolor='', c='0.5')
    ax.plot(np.arange(-5,5,0.01), np.arange(-5,5,0.01) *mod2.params['sm_change'], c='k')
    ax.set_xlim([-0.15, 0.15])
    ax.set_ylim([-10, 10])
    ax.set_xlabel('Soil moisture change (m$^2$/m$^2$)')
    ax.set_ylabel('AGB change (tC/ha/yr)')
    ax.grid()
    plt.show()
    
    return mod2.params['sm_change']
    


if __name__ == '__main__':
    
    data_dir = '/home/sbowers3/guasha/sam_bowers/carla_outputs/'

    t1 = 2007
    t2 = 2010
    
    # Load data
    AGB_t1, AGB_t2, sm_t1, sm_t2 = loadCSV(data_dir, t1, t2, downsample = 100)
    
    # Fit model and plot
    slope = scatterPlot3(AGB_t1, AGB_t2, sm_t1, sm_t2)
    
    
    
