
import glob
import numpy as np
import os
from osgeo import gdal
import skimage

import biota

import pdb


def getFiles(t1, t2, data_dir = os.getcwd()):
    '''
    Find files in data_dir that match name_pattern for years t1 and t2. Reduce to only files that are present for both t1 and t2.
    
    Args:
        t1: a year (integer)
        t2: a year (integer)
        data_dir: directory containing data
        
    Returns:
        A list of files and t1, and a list of files at t2m, a list of treecover files, a list of soil moisture change
    '''
    
    files_t1 = np.array(sorted(glob.glob('%s/%s_%s_???????.tif'%(data_dir, 'AGB', str(t1)))))
    files_t2 = np.array(sorted(glob.glob('%s/%s_%s_???????.tif'%(data_dir, 'AGB', str(t2)))))
    files_t2adj = np.array(sorted(glob.glob('%s/%s_%s_???????.tif'%(data_dir, 'AGBadj', str(t2)))))
    files_treecover = np.array(sorted(glob.glob('%s/%s_???????.tif'%(data_dir, 'treecover'))))
    files_smchange = np.array(sorted(glob.glob('%s/%s_%s_%s_???????.tif'%(data_dir, 'SMChange', str(t1), str(t2)))))


    ids_t1 = np.array([this_file.split('_')[-1][:-4] for this_file in files_t1])
    ids_t2 = np.array([this_file.split('_')[-1][:-4] for this_file in files_t2])
    ids_t2adj = np.array([this_file.split('_')[-1][:-4] for this_file in files_t2adj])
    ids_treecover = np.array([this_file.split('_')[-1][:-4] for this_file in files_treecover])
    ids_smchange = np.array([this_file.split('_')[-1][:-4] for this_file in files_smchange])
    
    shared_ids = np.intersect1d(np.intersect1d(np.intersect1d(np.intersect1d(ids_t1, ids_t2), ids_treecover), ids_smchange), ids_t2adj)
    
    files_t1_reduced = files_t1[np.in1d(ids_t1, shared_ids)]
    files_t2_reduced = files_t2[np.in1d(ids_t2, shared_ids)]
    files_t2adj_reduced = files_t2adj[np.in1d(ids_t2adj, shared_ids)]
    files_treecover_reduced = files_treecover[np.in1d(ids_treecover, shared_ids)]
    files_smchange_reduced = files_smchange[np.in1d(ids_smchange, shared_ids)]
    
    return files_t1_reduced.tolist(), files_t2_reduced.tolist(), files_t2adj_reduced.tolist(), files_treecover_reduced.tolist(), files_smchange_reduced.tolist()


def getMetadata(input_files, downsample_factor):
    '''
    Determine output metadata from list of input files
    
    Args:
        input_files: List of input files from biota
        downsample_factor: Downsampling factor for output (e.g. 45 for 1.125 km res)
     
     Returns:
         extent, GeoTransform, projection
    '''
    
    xmin_t, ymin_t, xmax_t, ymax_t = None, None, None, None
    
    for input_file in input_files:
        
        ds = gdal.Open(input_file, 0)
        geo_t = ds.GetGeoTransform()
        
        xmin, xmax = geo_t[0], geo_t[0] + geo_t[1] * ds.RasterXSize
        ymax, ymin = geo_t[3], geo_t[3] + geo_t[5] * ds.RasterYSize
        
        if xmin_t is None or xmin < xmin_t:
            xmin_t = xmin
        if xmax_t is None or xmax > xmax_t:
            xmax_t = xmax
        if ymin_t is None or ymin < ymin_t:
            ymin_t = ymin
        if ymax_t is None or ymax > ymax_t:
            ymax_t = ymax
    
    xres, yres = geo_t[1] * downsample_factor, geo_t[5] * downsample_factor
    
    geo_t = xmin_t, xres, 0., ymax_t, 0., yres
    
    proj = ds.GetProjection()
    
    return [xmin_t, ymin_t, xmax_t, ymax_t], geo_t, proj



def downsampleArray(ds, downsample_factor, p_accept = 0.5, downsample_type = 'mean', percentile = 95):
    '''
    Args:
        ds: A gdal dataset
        downsample_factor: Downsampling factor for output (e.g. 45 for 1.125 km res)
        p_accept: Set proportion of px thjat must be available in grid cell to accept measurement
    
    Returns:
        A numpy array
    '''
    
    #def percentile_func(block, axis, percentile = percentile):
    #    print block.shape
    #    percentile_tweak = percentile + ((100 - percentile) * (float(np.sum(block ==0)) / block.shape[0]))
    #    return np.percentile(block, percentile_tweak, axis=0)#np.percentile(block, 95)
        
    assert downsample_type in ['mean', 'percentile'], "downsample_type must be 'mean' or 'percentile'."
    
    nodata = ds.GetRasterBand(1).GetNoDataValue()
    
    data = ds.ReadAsArray().astype(np.float32)
    mask = data == nodata
    data[mask] = 0.

    # Calculate sum of data
    data_sum = skimage.measure.block_reduce(data, (downsample_factor, downsample_factor), np.sum)
    
    # Calculate sum of mask
    valid_px_sum = (downsample_factor ** 2) - skimage.measure.block_reduce(mask, (downsample_factor, downsample_factor), np.sum)
    
    if downsample_type == 'mean':
                # Calculate mean
        data_downsampled = np.zeros_like(data_sum) + nodata
        sel = valid_px_sum >  (downsample_factor ** 2) * p_accept # Require at least 50 % data from a grid cell
        data_downsampled[sel] = data_sum[sel] / valid_px_sum[sel]
        data_downsampled = np.ma.array(data_downsampled, mask = data_downsampled == nodata)
    
    
    if downsample_type == 'percentile':
        
        #print 'Not implemented!'
        data_downsampled = np.zeros_like(data_sum) + nodata
        
        for y in range(int(float(ds.RasterYSize) / downsample_factor)):
            ymin = y * downsample_factor; ymax = ymin + downsample_factor
            for x in range(int(float(ds.RasterXSize) / downsample_factor)):
                xmin = x * downsample_factor; xmax = xmin + downsample_factor
                data_downsampled[y,x] = np.percentile(np.ma.array(data[ymin:ymax, xmin:xmax],mask=mask[ymin:ymax,xmin:xmax]), percentile)
        
        # Mask values < p_accept     
        sel = valid_px_sum > (downsample_factor ** 2) * p_accept
        data_downsampled[sel == False] = nodata
        data_downsampled = np.ma.array(data_downsampled, mask = data_downsampled == nodata)       
        
        # Calculate sum of mask
        #valid_px_sum = (downsample_factor ** 2) - skimage.measure.block_reduce(mask, (downsample_factor, downsample_factor), np.sum)
        #pdb.set_trace()
        #data_pc = skimage.measure.block_reduce(data, (downsample_factor, downsample_factor), percentile_func)
        
        # Calculate percentile
        #data_downsampled = np.zeros_like(data_sum) + nodata
        #sel = valid_px_sum >  (downsample_factor ** 2) * p_accept # Require at least 50 % data from a grid cell
    
    return data_downsampled


def buildOutputArray(extent, geo_t, dtype = np.float32, nodata = None):
    '''
    Build a blank output array
    
    Args:
        extent: Extent of output array
        geo_t: Output GeoTransform
        dtype: Numpy data type for output (defaults to float)        
        nodata: optionally set a nodata value
    Returns:
        An empty array       
    '''
    
    blank_array = np.ma.zeros((int((extent[1] - extent[3]) / geo_t[5]), int((extent[2] - extent[0]) / geo_t[1])), dtype = dtype)
    
    if nodata is not None:
        blank_array += nodata
        blank_array.mask = blank_array == nodata
    
    return blank_array


def buildMask(ds, geo_t_dest, extent_dest, downsample_factor):
    '''
    Builds a mask that located a tile within an overall image
    
    Args:
        ds: gdal dataset for input file
        geo_t_dest: destination GeoTransform()
        extent_dest: Extent of output array
        downsample_factor: Downsampling factor for output (e.g. 45 for 1.125 km res)
        
    Returns:
        A mask indicating the extent of an input image in output extent
    '''
    
    def world2Pixel(geo_t, x, y):
        """
        Uses a gdal geomatrix (gdal.GetGeoTransform()) to calculate
        the pixel location of a geospatial coordinate
        From: https://pcjericks.github.io/py-gdalogr-cookbook/raster_layers.html
        """
        ulX = geo_t[0]
        ulY = geo_t[3]
        xDist = geo_t[1]
        yDist = geo_t[5]
        pixel = int((x - ulX) / xDist)
        line = int((ulY - y) / xDist)
        return (pixel, line)
    
    # Generate source GeoTransform (with downsample factor)
    gts = ds.GetGeoTransform()
    geo_t_source = (gts[0], gts[1] * downsample_factor, gts[2], gts[3], gts[4], gts[5] * downsample_factor)
    
    # Calculate pixel extent of source image in output coordinates
    px_min, ln_min = world2Pixel(geo_t_dest, geo_t_source[0], geo_t_source[3])
    px_max, ln_max = world2Pixel(geo_t_dest, geo_t_source[0] + (geo_t_source[1] * (ds.RasterXSize/downsample_factor)), geo_t_source[3] + (geo_t_source[5] * (ds.RasterYSize/downsample_factor)))
    
    # Build a blank output array, and generate a mask for it
    output_array = buildOutputArray(extent_dest, geo_t_dest, dtype = np.bool)
    output_array[ln_min:ln_max, px_min:px_max] = True
    
    return output_array


def build_boxplot(agb_t1, agb_t2):
    '''
    Build a boxplot of AGB_t1 vs AGB change, and display
    
    Args:
        agb_t1: Masked array of AGB at time 1
        abg_t2: Masked array of AGB at time 2
    '''
    
    import matplotlib.pyplot as plt
    
    sel = np.logical_and(agb_t1.mask == False, agb_t2.mask == False)
    agb_t1 = agb_t1[sel]
    agb_t2 = agb_t2[sel]
    agb_change = agb_t2 - agb_t1
    
    # Calculate 5 tC/ha AGB classes
    agb_class = (np.round(agb_t1 / 5. , 0) * 5).astype(np.int)
    
    # Boxplot
    fig = plt.figure(figsize=(12, 6))
    ax = fig.add_subplot(111)
    ax.boxplot([agb_change[agb_class == i] for i in np.unique(agb_class[agb_class <= 60])])
    ax.grid(color = '0.5', linestyle = '--', which = 'major', axis='y')
    ax.set_xticklabels(np.unique(agb_class))
    ax.set_xlabel('AGB t1 (tC/ha)')
    ax.set_ylabel('AGB t2 - t1 (tC/ha)')
    plt.show()



if __name__ == '__main__':
    '''
    '''
    
    data_dir = '/home/sbowers3/guasha/sam_bowers/carla_outputs/'
    output_dir = '/home/sbowers3/DATA/bistable/Outputs/'
    
    t1 = 2007
    t2 = 2010
    downsample_factor = 45
    p_accept = 0.5 
    nodata = 999999.
    
    files_t1, files_t2, files_t2adj, files_treecover, files_smchange = getFiles(t1, t2, data_dir = data_dir)
    
    extent_out, geo_t_out, proj_out = getMetadata(files_t1, downsample_factor)
    
    AGB_t1_out = buildOutputArray(extent_out, geo_t_out, nodata = nodata)
    AGB_t2_out = buildOutputArray(extent_out, geo_t_out, nodata = nodata)
    AGB_t2adj_out = buildOutputArray(extent_out, geo_t_out, nodata = nodata)
    treecover_out = buildOutputArray(extent_out, geo_t_out, nodata = nodata)
    smchange_out = buildOutputArray(extent_out, geo_t_out, nodata = nodata)
    AGB_t1_95pc_out = buildOutputArray(extent_out, geo_t_out, nodata = nodata)
    AGB_t1_90pc_out = buildOutputArray(extent_out, geo_t_out, nodata = nodata)
    
    for file_t1, file_t2, file_t2adj, file_treecover, file_smchange in zip(files_t1, files_t2, files_t2adj, files_treecover, files_smchange):
        
        print 'Doing %s'%file_t1
        
        ds_t1 = gdal.Open(file_t1, 0)
        ds_t2 = gdal.Open(file_t2, 0)
        ds_t2adj = gdal.Open(file_t2adj, 0)
        ds_treecover = gdal.Open(file_treecover, 0)
        ds_smchange = gdal.Open(file_smchange, 0)

        #AGB_t1 = downsampleArray(ds_t1, downsample_factor, p_accept = p_accept)
        #AGB_t2 = downsampleArray(ds_t2, downsample_factor, p_accept = p_accept)
        #AGB_t2adj = downsampleArray(ds_t2adj, downsample_factor, p_accept = p_accept)
        #treecover = downsampleArray(ds_treecover, downsample_factor, p_accept = p_accept)
        #smchange = downsampleArray(ds_smchange, downsample_factor, p_accept = p_accept)
        
        AGB_t1_95pc = downsampleArray(ds_t1, downsample_factor, p_accept = p_accept, downsample_type = 'percentile', percentile = 95)
        AGB_t1_90pc = downsampleArray(ds_t1, downsample_factor, p_accept = p_accept, downsample_type = 'percentile', percentile = 90)
        
        tile_mask = buildMask(ds_t1, geo_t_out, extent_out, downsample_factor)
        print file_t1, file_t2
        
        if tile_mask.sum() == 0:
            continue
        
        #AGB_t1_out[tile_mask] = AGB_t1.flatten()
        #AGB_t2_out[tile_mask] = AGB_t2.flatten()
        #AGB_t2adj_out[tile_mask] = AGB_t2adj.flatten()
        #treecover_out[tile_mask] = treecover.flatten()
        #smchange_out[tile_mask] = smchange.flatten()
        AGB_t1_95pc_out[tile_mask] = AGB_t1_95pc.flatten()
        AGB_t1_90pc_out[tile_mask] = AGB_t1_90pc.flatten()

    # Output GeoTiffs
    #biota.IO.outputGeoTiff(AGB_t1_out, 'AGB_2007.tif', geo_t_out, proj_out, output_dir = output_dir, dtype = 6, nodata = nodata)
    #biota.IO.outputGeoTiff(AGB_t2_out, 'AGB_2010.tif', geo_t_out, proj_out, output_dir = output_dir, dtype = 6, nodata = nodata)
    #biota.IO.outputGeoTiff(AGB_t2adj_out, 'AGBadj_2010.tif', geo_t_out, proj_out, output_dir = output_dir, dtype = 6, nodata = nodata)
    #biota.IO.outputGeoTiff(AGB_t2_out - AGB_t1_out, 'AGBChange_2007_2010.tif', geo_t_out, proj_out, output_dir = output_dir, dtype = 6, nodata = nodata)
    #biota.IO.outputGeoTiff(AGB_t2adj_out - AGB_t1_out, 'AGBChange_2007_2010adj.tif', geo_t_out, proj_out, output_dir = output_dir, dtype = 6, nodata = nodata)
    #biota.IO.outputGeoTiff(treecover_out, 'treecover.tif', geo_t_out, proj_out, output_dir = output_dir, dtype = 6, nodata = nodata)
    #biota.IO.outputGeoTiff(smchange_out, 'SMChange_2007_2010.tif', geo_t_out, proj_out, output_dir = output_dir, dtype = 6, nodata = nodata)
    biota.IO.outputGeoTiff(AGB_t1_95pc_out, 'AGB_95pc_2007.tif', geo_t_out, proj_out, output_dir = output_dir, dtype = 6, nodata = nodata)
    biota.IO.outputGeoTiff(AGB_t1_90pc_out, 'AGB_90pc_2007.tif', geo_t_out, proj_out, output_dir = output_dir, dtype = 6, nodata = nodata)

    #build_boxplot(AGB_t1_out, AGB_t2_out)
