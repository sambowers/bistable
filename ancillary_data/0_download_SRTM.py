# Download SRTM data

import os

output_dir = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/srtm/'

os.chdir(output_dir)

lats = range(-35, 1) #range(-15,-5) #
lons = range(0, 46)
    
for lat in lats:
    
    if lat >=0:
        hem_NS = 'N'
    else:
        hem_NS = 'S'
    
    for lon in lons:
        
        if lon >=0:
            hem_EW = 'E'
        else:
            hem_EW = 'W'
            
        filename = '%s%s%s%s.hgt.zip'%(hem_NS, str(abs(lat)).zfill(2), hem_EW, str(abs(lon)).zfill(3))
        
        filepath = 'https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Africa/%s'%filename
        
        os.system('wget %s'%filepath)
        os.system('unzip %s'%filename)
        os.system('rm %s'%filename)
        
