import os
from osgeo import gdal
import glob

data_dir = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/white_map'
input_tif = 'whiteveg.bil'
output_dir = '/home/sbowers3/DATA/bistable/Outputs'
africa_tif = '/home/sbowers3/DATA/bistable/Outputs/AGB_2010.tif'

# Write output CRS
os.system('gdalsrsinfo -o wkt %s > %s/Africa.wkt'%(africa_tif, data_dir))

# Find output extent
ds = gdal.Open(africa_tif, 0)
ulx, xres, xskew, uly, yskew, yres  = ds.GetGeoTransform()
lrx = ulx + (ds.RasterXSize * xres)
lry = uly + (ds.RasterYSize * yres)
extent = '%s %s %s %s'%(str(ulx), str(lry), str(lrx), str(uly))

# Reproject
os.system('gdalwarp -t_srs %s/Africa.wkt -r near -overwrite -co COMPRESS=LZW -te %s -tr %s %s %s/%s %s/white_map.tif'%(data_dir, extent, str(xres), str(yres), data_dir, input_tif, output_dir))
