import numpy as np
from osgeo import gdal

import biota

import pdb

# Options
wdpa = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/protected_areas/WDPA_July2018-shapefile-polygons_CUT.shp'
africa_tif = '/home/sbowers3/DATA/bistable/Outputs/AGB_2010.tif'
output_dir = '/home/sbowers3/DATA/bistable/Outputs/'

class DummyTile(object):
    """
    This class allows maskShapefile to understand a standard geoTiff
    """
        
    def __init__(self, geoTiff):
        """
        Loads data and metadata from a geoTiff to mimic an ALOS tile
        """
                
        from osgeo import gdal
        
        ds = gdal.Open(geoTiff)
        
        self.geo_t = ds.GetGeoTransform()
        self.proj = ds.GetProjection()
        self.xSize = ds.RasterXSize
        self.ySize = ds.RasterYSize
        self.xRes = 1
        self.yRes = 1
        

# Load different categories of protection
IUCN_Ia = biota.mask.maskShapefile(DummyTile(africa_tif), wdpa, field = 'IUCN_CAT', value = 'Ia')
IUCN_Ib = biota.mask.maskShapefile(DummyTile(africa_tif), wdpa, field = 'IUCN_CAT', value = 'Ib')
IUCN_II = biota.mask.maskShapefile(DummyTile(africa_tif), wdpa, field = 'IUCN_CAT', value = 'II')
IUCN_III = biota.mask.maskShapefile(DummyTile(africa_tif), wdpa, field = 'IUCN_CAT', value = 'III')
IUCN_IV = biota.mask.maskShapefile(DummyTile(africa_tif), wdpa, field = 'IUCN_CAT', value = 'IV')
IUCN_V  = biota.mask.maskShapefile(DummyTile(africa_tif), wdpa, field = 'IUCN_CAT', value = 'V')
IUCN_VI = biota.mask.maskShapefile(DummyTile(africa_tif), wdpa, field = 'IUCN_CAT', value = 'VI')
all_protection = biota.mask.maskShapefile(DummyTile(africa_tif), wdpa)

# Post-hoc categorize
#strict_protection = IUCN_II | IUCN_Ib | IUCN_Ia
#management_area = IUCN_III | IUCN_IV
#human_area = IUCN_V | IUCN_VI

# Build a classified output
output = np.zeros_like(all_protection, dtype = np.uint8)
output[all_protection] = 7
output[IUCN_VI] = 6
output[IUCN_V] = 5
output[IUCN_IV] = 4
output[IUCN_III] = 3
output[IUCN_II] = 2
output[IUCN_Ib | IUCN_Ia] = 1

# Save to GeoTiff
biota.IO.outputGeoTiff(output, 'protected_areas_IUCN.tif', DummyTile(africa_tif).geo_t, DummyTile(africa_tif).proj, output_dir = output_dir, dtype = gdal.GDT_Byte) 


# Get WDPA ID number (for John)
output2 = np.zeros_like(all_protection, dtype = np.int32)

# Prioritise highest level of protection
PIDs = biota.mask.getField(wdpa, 'WDPA_PID')
IUCN_class = biota.mask.getField(wdpa, 'IUCN_CAT')

for IUCN in ['Not Applicable', 'Not Assigned','Not Reported', 'VI', 'V', 'IV', 'III', 'II', 'Ib', 'Ia']:
    for PID in PIDs[IUCN_class == IUCN]:
        print PID
        pid = biota.mask.maskShapefile(DummyTile(africa_tif), wdpa, field = 'WDPA_PID', value = PID)
        try:
            output2[pid] = int(PID)
        except:
            print 'Failed'
    
biota.IO.outputGeoTiff(output2, 'protected_areas_WDPA_ID.tif', DummyTile(africa_tif).geo_t, DummyTile(africa_tif).proj, output_dir = output_dir, dtype = gdal.GDT_Int32)

