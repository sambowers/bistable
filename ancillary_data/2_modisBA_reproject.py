import os
from osgeo import gdal

data_dir = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/fire/modis_ba'
output_dir = '/home/sbowers3/DATA/bistable/Outputs'
africa_tif = '/home/sbowers3/DATA/bistable/Outputs/AGB_2010.tif'

year_min, year_max = 2007, 2010 #Inclusive

input_files = '%s/Win12_firecount_%s_%s.tif %s/Win13_firecount_%s_%s.tif %s/Win14_firecount_%s_%s.tif'%(data_dir, str(year_min), str(year_max), data_dir, str(year_min), str(year_max),data_dir, str(year_min), str(year_max))

# Merge three windows
os.system('gdal_merge.py -o %s/All_firecount_%s_%s.tif %s'%(data_dir, str(year_min), str(year_max), input_files))

# Write output CRS
os.system('gdalsrsinfo -o wkt %s > %s/Africa.wkt'%(africa_tif, data_dir))

# Find output extent
ds = gdal.Open(africa_tif, 0)
ulx, xres, xskew, uly, yskew, yres  = ds.GetGeoTransform()
lrx = ulx + (ds.RasterXSize * xres)
lry = uly + (ds.RasterYSize * yres)
extent = '%s %s %s %s'%(str(ulx), str(lry), str(lrx), str(uly))

# Reproject
os.system('gdalwarp -t_srs %s/Africa.wkt -r average -overwrite -co COMPRESS=LZW -te %s -tr %s %s %s/All_firecount_%s_%s.tif %s/firecount_%s_%s.tif'%(data_dir, extent, str(xres), str(yres), data_dir, str(year_min), str(year_max), output_dir, str(year_min), str(year_max)))
