import numpy as np
from osgeo import gdal
import csv

import pdb

# Options
modis_af = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/fire/modis_af/fire_archive_M6_23113.csv'
africa_tif = '/home/sbowers3/DATA/bistable/Outputs/AGB_2010.tif'
output_dir = '/home/sbowers3/DATA/bistable/Outputs/'


global pixel
global line
global frp


# Load CSV
lat, lon, frp = [], [], []

with open(modis_af, 'rb') as csvfile:
    reader = csv.reader(csvfile)
    header = reader.next()
    for row in reader:
        lat.append(float(row[0]))
        lon.append(float(row[1]))
        frp.append(float(row[-1]))

lat = np.array(lat)
lon = np.array(lon)
frp = np.array(frp)


# Find output extent
ds = gdal.Open(africa_tif, 0)
#fire_count = np.zeros((ds.RasterYSize,ds.RasterXSize)).astype(np.int32) + 9999999
#fire_sum = np.zeros((ds.RasterYSize,ds.RasterXSize)).astype(np.float32) + 9999999.
#fire_mean = np.zeros((ds.RasterYSize,ds.RasterXSize)).astype(np.float32) + 9999999.

def world2Pixel(geo_t, lat, lon):
    '''
    '''
    pixel = np.round((lon - geo_t[0]) / geo_t[1], 0).astype(np.int)
    line = np.round((geo_t[3] - lat) / geo_t[1], 0).astype(np.int)
    
    return pixel, line

pixel, line = world2Pixel(ds.GetGeoTransform(), lat, lon)

sel = np.logical_and(np.logical_and(pixel >= 0, pixel < ds.RasterXSize), np.logical_and(line >= 0, line < ds.RasterYSize))
pixel = pixel[sel]
line = line[sel]
frp = frp[sel]

## Calculate metrics

fire_count = np.histogram2d(line, pixel, bins=(np.arange(0,ds.RasterYSize+1,1), np.arange(0,ds.RasterXSize+1,1)))[0]
fire_sum = np.histogram2d(line, pixel, bins=(np.arange(0,ds.RasterYSize+1,1), np.arange(0,ds.RasterXSize+1,1)), weights = frp)[0]

fire_mean = np.zeros_like(fire_sum)
fire_mean[fire_count > 0] = fire_sum[fire_count > 0] / fire_count[fire_count > 0]



# Output fire_count
driver = gdal.GetDriverByName('GTiff')
ds_out= driver.Create('%s/%s'%(output_dir, 'AFcount_2001_2018.tif'), fire_count.shape[1], fire_count.shape[0], 1, gdal.GDT_Int32, options = ['COMPRESS=LZW'])
ds_out.SetGeoTransform(ds.GetGeoTransform())
ds_out.SetProjection(ds.GetProjection())
#ds_out.GetRasterBand(1).SetNoDataValue(9999999)
ds_out.GetRasterBand(1).WriteArray(fire_count)#.filled(9999999))
ds_out = None


# Output frp sum
driver = gdal.GetDriverByName('GTiff')
ds_out= driver.Create('%s/%s'%(output_dir, 'AFfrpsum_2001_2018.tif'), fire_sum.shape[1], fire_sum.shape[0], 1, gdal.GDT_Int32, options = ['COMPRESS=LZW'])
ds_out.SetGeoTransform(ds.GetGeoTransform())
ds_out.SetProjection(ds.GetProjection())
#ds_out.GetRasterBand(1).SetNoDataValue(9999999.)
ds_out.GetRasterBand(1).WriteArray(fire_sum)#.filled(9999999.))
ds_out = None

# Output fire_mean
driver = gdal.GetDriverByName('GTiff')
ds_out= driver.Create('%s/%s'%(output_dir, 'AFfrpmean_2001_2018.tif'), fire_mean.shape[1], fire_mean.shape[0], 1, gdal.GDT_Int32, options = ['COMPRESS=LZW'])
ds_out.SetGeoTransform(ds.GetGeoTransform())
ds_out.SetProjection(ds.GetProjection())
#ds_out.GetRasterBand(1).SetNoDataValue(9999999.)
ds_out.GetRasterBand(1).WriteArray(fire_mean)#.filled(9999999.))
ds_out = None

