import os
from osgeo import gdal
import glob

data_dir = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/livestock'
output_dir = '/home/sbowers3/DATA/bistable/Outputs'
africa_tif = '/home/sbowers3/DATA/bistable/Outputs/AGB_2010.tif'

# Write output CRS
os.system('gdalsrsinfo -o wkt %s > %s/Africa.wkt'%(africa_tif, data_dir))

# Find output extent
ds = gdal.Open(africa_tif, 0)
ulx, xres, xskew, uly, yskew, yres  = ds.GetGeoTransform()
lrx = ulx + (ds.RasterXSize * xres)
lry = uly + (ds.RasterYSize * yres)
extent = '%s %s %s %s'%(str(ulx), str(lry), str(lrx), str(uly))

for sp, species in zip(['ct', 'gt', 'sh'], ['cattle', 'goats', 'sheep']):
    
    input_files = glob.glob('%s/glb%s*/GLiPHAmaps/global/%s/*/*/w001001.adf'%(data_dir, sp, species))
    
    for input_file in input_files:
            
        flavour = input_file.split('/')[-3]
    
        # Reproject
        os.system('gdalwarp -t_srs %s/Africa.wkt -r average -overwrite -co COMPRESS=LZW -te %s -tr %s %s %s %s/%s_%s.tif'%(data_dir, extent, str(xres), str(yres), input_file, output_dir, species, flavour))
