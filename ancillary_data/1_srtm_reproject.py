import os
from osgeo import gdal
import glob
import numpy as np
import scipy.interpolate
import scipy.ndimage.filters

import pdb

# Calculate standard deviation with a moving window and a masked array
"""
def window_stdev(img, window_size = 3):
    '''
    Based on https://stackoverflow.com/questions/18419871/improving-code-efficiency-standard-deviation-on-sliding-windows
    and http://nickc1.github.io/python,/matlab/2016/05/17/Standard-Deviation-(Filters)-in-Matlab-and-Python.html
    '''
    
    from scipy import signal
    import scipy.ndimage.morphology
    
    indices = scipy.ndimage.morphology.distance_transform_edt(img.mask, return_distances = False, return_indices = True)
    data = img.data[tuple(indices)].astype(np.float32)
    
    c1 = signal.convolve2d(data, np.ones((window_size, window_size)) / (window_size ** 2), boundary = 'symm')
    c2 = signal.convolve2d(data*data, np.ones((window_size, window_size)) / (window_size ** 2), boundary = 'symm')
    
    border = window_size / 2
    
    variance = c2 - c1 * c1
    variance[variance < 0] += 0.01 # Prevents divide by zero errors.
    
    pdb.set_trace()
    
    img_out = np.sqrt(variance)[border:-border, border:-border]
    
    img_out[np.isfinite(img_out) == False] = 0
        
    return np.ma.array(img_out, mask = np.logical_or(np.isfinite(img_out) == False, img.mask))
"""

def window_stdev(arr, window_size = 3):
    # Convolution window that records standard deviation
    c1 = scipy.ndimage.filters.uniform_filter(arr, window_size*2, mode='constant', origin=-window_size)
    c2 = scipy.ndimage.filters.uniform_filter(arr*arr, window_size*2, mode='constant', origin=-window_size)
    return ((c2 - c1*c1)**.5)[:-window_size*2+1,:-window_size*2+1]


data_dir = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/srtm/'
output_dir = '/home/sbowers3/DATA/bistable/Outputs/'
africa_tif = '/home/sbowers3/DATA/bistable/Outputs/AGB_2010.tif'


input_files = glob.glob('%s*.hgt'%data_dir)

# Merge tiles
os.system('gdal_merge.py -o %s/SRTM_merge.tif %s'%(data_dir, ' '.join(input_files)))

# Write output CRS
os.system('gdalsrsinfo -o wkt %s > %s/Africa.wkt'%(africa_tif, data_dir))

# Find output extent
ds = gdal.Open(africa_tif, 0)
ulx, xres, xskew, uly, yskew, yres  = ds.GetGeoTransform()
lrx = ulx + (ds.RasterXSize * xres)
lry = uly + (ds.RasterYSize * yres)
extent = '%s %s %s %s'%(str(ulx), str(lry), str(lrx), str(uly))

# Reproject elevation
os.system('gdalwarp -t_srs %s/Africa.wkt -r average -overwrite -co COMPRESS=LZW -te %s -tr %s %s %s/SRTM_merge.tif %s/elevation.tif'%(data_dir, extent, str(xres), str(yres), data_dir, output_dir))

# Load into memory
ds_src = gdal.Open('%s/SRTM_merge.tif'%data_dir,0)
data_src = ds_src.ReadAsArray()
data_src = np.ma.array(data_src, mask = data_src == 0, dtype = np.float32)


# Calculate standard deviation over 3x3 window
data_std = window_stdev(data_src, window_size = 3)

# Filter cuts off boundaries, re-fill.
data_std_out = np.zeros_like(data_src)
data_std_out.data[2:-3,2:-3] = data_std


# Output standard deviation
driver = gdal.GetDriverByName('GTiff')
ds = driver.Create('%s/%s'%(data_dir, 'SRTM_std.tif'), data_std_out.shape[1], data_std_out.shape[0], 1, gdal.GDT_Float32, options = ['COMPRESS=LZW'])
ds.SetGeoTransform(ds_src.GetGeoTransform())
ds.SetProjection(ds_src.GetProjection())
ds.GetRasterBand(1).SetNoDataValue(99999.)
ds.GetRasterBand(1).WriteArray(data_std_out.filled(99999.))
ds = None

# Reproject roughness
os.system('gdalwarp -t_srs %s/Africa.wkt -r average -overwrite -co COMPRESS=LZW -te %s -tr %s %s %s/SRTM_std.tif %s/topographic_roughness.tif'%(data_dir, extent, str(xres), str(yres), data_dir, output_dir))