from ftplib import FTP

output_dir = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/fire/modis_ba/'

ftp = FTP('ba1.geog.umd.edu')
ftp.login(user='user',passwd='burnt_data')

for win in [12, 13, 14]:
    for year in range(2001,2018,1):
        ftp.cwd('Collection6/TIFF/Win%s/%s/'%(str(win).zfill(2), str(year)))
        for remote_file in ftp.nlst():
            if 'burndate.tif' not in remote_file:
                pass
            else:
                print 'Doing %s'%remote_file
                local_filename = '%s/%s'%(output_dir,remote_file)
                local_file = open(local_filename, 'wb')
                ftp.retrbinary('RETR %s' %remote_file, local_file.write)
                local_file.close()
        ftp.cwd('~')

ftp.close()
            





ftp.close()
