from osgeo import gdal
import numpy as np
import glob
import biota

import pdb

data_dir = '/home/sbowers3/guasha/sam_bowers/carla_outputs/DATA/fire/modis_ba/'

year_min, year_max = 2007, 2010 #Inclusive
windows = [12, 13, 14]


for win in windows:
    
    files = sorted(glob.glob(data_dir + 'MCD64monthly.*.Win%s.*.burndate.tif'%str(win)))
    
    fire_count = np.zeros_like(gdal.Open(files[0]).ReadAsArray(), dtype = np.uint8)
    
    for this_file in files:
        year = int(this_file.split('.')[1][1:5])
        if year < year_min or year > year_max:
            continue
        print 'Reading %s'%this_file
        ds = gdal.Open(this_file, 0)
        data = ds.ReadAsArray()
        fire_count += (np.logical_and(data > 0, data < 366) * 1).astype(np.uint8)
    
    biota.IO.outputGeoTiff(fire_count, 'Win%s_firecount_%s_%s.tif'%(str(win), str(year_min), str(year_max)), ds.GetGeoTransform(), ds.GetProjection(), output_dir = data_dir, dtype = gdal.GDT_Byte) 
