# README #

### What is this repository for? ###

These scripts are used for identifying a savannah-forest feedback in ALOS data.

### How do I get set up? ###

Modules are available in Anaconda Python. You'll also need the Python library named 'biota' (https://bitbucket.org/sambowers/biota/src).

### Who do I talk to? ###

Samuel Bowers (sam.bowers@ed.ac.uk)